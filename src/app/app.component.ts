import { Component } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
// import { Geolocation } from '@ionic-native/geolocation';
import { Autostart } from '@ionic-native/autostart';
// import { Slides } from 'ionic-angular';
// import { Camera, CameraOptions } from '@ionic-native/camera';
// import { AppVersion } from '@ionic-native/app-version';


import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;
  public base64Image: string;

  constructor(platform: Platform
      , statusBar: StatusBar
      , public alertCtrl: AlertController
      , private autostart: Autostart
      // , private camera: Camera
    ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      this.autostart.enable();
    });

  };
  
  // takePicture(){
  //   this.camera.getPicture({
  //       destinationType: this.camera.DestinationType.DATA_URL,
  //       targetWidth: 1000,
  //       targetHeight: 1000
  //   }).then((imageData) => {
  //     // imageData is a base64 encoded string
  //       this.base64Image = "data:image/jpeg;base64," + imageData;
  //   }, (err) => {
  //       console.log(err);
  //   });
  // }
};