import { BrowserModule } from '@angular/platform-browser';
// import { Platform, AlertController } from 'ionic-angular';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Firebase } from '@ionic-native/firebase';
import { Geolocation } from '@ionic-native/geolocation';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
// import { AppVersion } from '@ionic-native/app-version';


@NgModule({
  declarations: [
    MyApp,
    Firebase,
    Geolocation,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Firebase,
    Geolocation,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Firebase,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})

export class AppModule {
  constructor(public firebase: Firebase) { }
  
  //collect Analytics
  protected collectAnalytics(contentType: string, itemId: string): void {
    this.firebase.logEvent("select_content", { content_type: contentType, item_id: itemId })
      .then((success) => console.log(`success`))
      .catch(error => console.error('Error', error));

      this.firebase.getToken()
      .then(token => console.log(`The token is ${token}`)) // save the token server-side and use it to push notifications to this device
      .catch(error => console.error('Error getting token', error));

      this.firebase.onTokenRefresh()
      .subscribe((token: string) => console.log(`Got a new token ${token}`));
  }
}

