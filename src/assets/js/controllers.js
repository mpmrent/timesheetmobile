
'use strict';

// var server = "http://timesheet.mpm-rent.net/";
// var server = "http://192.168.22.113:8100/";
var server = "http://182.23.44.169/";
// var server = "http://localhost/";
var tmsService = angular.module('tmsService',['ionic']);
var user = "-";

function dataURItoBlob(dataURI) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);

    // create a view into the buffer
    var ia = new Uint8Array(ab);

    // set the bytes of the buffer to the correct values
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    // write the ArrayBuffer to a blob, and you're done
    var blob = new Blob([ab], {type: mimeString});
    return blob;

}

function getFormattedDate(date) {
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return year + '-' + month + '-' + day;
}

function getFormattedTime(date) {
    var hour = date.getHours();
    var minute = date.getMinutes();
    var second = date.getSeconds();
    return hour + ':' + minute + ':' + second;
}

function radioChecked(val) {
    for (var i = 1; i <= 4; i++) { 
        document.getElementById('radioImg'+i).src='assets/img/Bintang-Abu.png';
    }
    for (var i = 1; i <= val; i++) { 
        document.getElementById('radioImg'+i).src='assets/img/Bintang-kuning.png';
    }
}

function alertButton() {
        var cek = 1;
        var counter = 0;
        var i = setInterval(function(){
            // do your thing
            if (cek) {
                try {
                    document.getElementById('backAgree').style.backgroundColor='#f47e3d';
                    document.getElementById('textAgree').style.color='white';
                } catch (e){
                    console.log(e.message);
                }
                cek = 0;
            } else {
                try {
                    document.getElementById('backAgree').style.backgroundColor='white';
                    document.getElementById('textAgree').style.color='#f47e3d';
                } catch (e){
                    console.log(e.message);
                }
                cek = 1;
            }
            counter++;
            if(counter === 10) {
                clearInterval(i);
            }
        }, 200);
}

// var app = require('express')();
// var http = require('http').Server(app);
// var io = require('socket.io')(http);

// io.on('connection', function(socket){
//     console.log('a user connected');
// });

// http.listen(3000, function(){
//     console.log('listening on *:3000');
// });

// var app    = express();
// var server = app.listen(3003);
// var io     = require('socket.io').listen(server);

// var socket = io( 'http://timesheet.mpm-rent.net:3003' );
var socket = io.connect( 'http://timesheet.mpm-rent.net:3003', {path: 'socket.io'} );
// var socket = io.connect( 'http://36.37.122.106:3003', {path: '/socket.io'} );
// var io = require('socket.io-client');

socket.heartbeatTimeout = 20000;
var deviceInfo = {};
    
angular.module('tms.controllers', [])
        .run(function($ionicPlatform) {
            $ionicPlatform.ready(function() {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
            });
        })
        .run(function(ClockSrv, tmsService, $ionicPopup, $ionicLoading, $window, $ionicHistory){
            try {
                var datas = $window.localStorage['username'];
                tmsService.getMessage(datas).success(function (response) {
                    try {
                        cordova.plugins.notification.local.requestPermission(function (granted) {
                            cordova.plugins.notification.local.schedule(response);
                        });
                        
                    } catch (e) {

                    }
                });
                socket.on( 'notif_bc', function( data ) {
                    try {
                        tmsService.getMessage(datas).success(function (response) {
                            console.log(response);
                            cordova.plugins.notification.local.requestPermission(function (granted) {
                                cordova.plugins.notification.local.schedule(response);
                            });
                        });
                    } catch(ex) {
                        console.log(ex);
                    }
                });
            } catch (ex) {
                console.log(ex.message);
            }
            

        })
        .controller('HomeCtrl', function ($scope, tmsService, $ionicPopup, $ionicLoading, $window, $ionicPlatform, $ionicHistory) {
            try {
                $window.localStorage['uid'] = "1654364x564sdfsdfrtR?!@323#Sd%";
                $window.localStorage['token'] = null;
                $window.localStorage['location'] = null;
                $window.localStorage['locationName'] = null;
                $window.localStorage['version_app'] = null;
                $window.localStorage['provinces'] = '';
                $window.localStorage['req_update'] = 0;

                ionic.Platform.ready(function(){
                    var device = ionic.Platform.device();
                    $window.localStorage['uuid'] = device.uuid;
                    $window.localStorage['model'] = device.model;
                    $window.localStorage['manufacturer'] = device.manufacturer;
                    $window.localStorage['version'] = device.version;
                    $window.localStorage['platform'] = device.platform;

                    deviceInfo = {
                        uuid : device.uuid,
                        model : device.model,
                        manufacturer : device.manufacturer,
                        version : device.version,
                        platform : device.platform
                    };
                    try {
                        cordova.getAppVersion.getVersionNumber().then(function (version) {
                            // var appVersion = version;
                            $scope.getVersionCode = version;
                            $window.localStorage['version_app'] = version;
                            tmsService.checkUpdate($window.localStorage['version_app']).success(function (response) {
                                console.log(response);
                                if (response.row == 1) {
                                    $scope.showAllert({
                                        title: '<i class="icon ion-alert"></i> Informasi Pembaharuan Aplikasi !',
                                        message: response.update_desc.replace(/(?:\r\n|\r|\n)/g, '<br />').replace('<br /><br />', '<hr><br />')
                                    });
                                    $window.localStorage['req_update'] = 1;
                                }
                            });
                        });
                    } catch (e) {
                        console.log( e.message);
                    }
                });

                $scope.showAllert = function (msg) {
                    $ionicPopup.alert({
                        title: msg.title,
                        template: msg.message,
                        okText: 'Ok',
                        okType: 'button-positive'
                    });
                };

                $ionicPlatform.registerBackButtonAction(function (e) {
                    e.preventDefault();
                    $ionicHistory.goBack();
                    return false;
                }, 100);

                $scope.data = {};
                $scope.data.uname = $window.localStorage['username'];
                $scope.data.passwd = $window.localStorage['password'];
                // console.log($window.localStorage);
                $scope.camera = function () {
                    $window.location.href = '#/camera';
                };
                tmsService.checkUpdate($window.localStorage['version_app']).success(function (response) {
                    console.log(response);
                    if (response.row == 1) {
                        $scope.showAllert({
                            title: '<i class="icon ion-alert"></i> Informasi Pembaharuan Aplikasi !',
                            message: response.update_desc
                        });
                        $window.localStorage['req_update'] = 1;
                    }
                });

                
                $scope.login = function () {
                    if ($window.localStorage['req_update'] == 1) {
                        $scope.showAllert({
                            title: '<i class="icon ion-alert"></i> INFORMASI',
                            message: 'Versi tidak mendukung, mohon untuk menguninstal dan install kembali melalui playstore.'
                        });
                    } else if (!$scope.data.uname) {
                        $scope.showAllert({
                            title: '<i class="icon ion-alert"></i> ALERT',
                            message: 'Driver ID must not null'
                        });
                    } else if (!$scope.data.passwd) {
                        $scope.showAllert({
                            title: '<i class="icon ion-alert"></i> ALERT',
                            message: 'Password must not null'
                        });
                    } else {
                        var data = {
                            uname: $scope.data.uname,
                            passwd: $scope.data.passwd
                        };
                        $scope.show = function () {
                            $ionicLoading.show({
                                template: 'Signing In...'
                            });
                        };
                        $scope.hide = function () {
                            $ionicLoading.hide();
                        };
                        $scope.show();

                        // $scope.agreeChecked = function() {
                        //     var data = {
                        //         username: $window.localStorage['username'],
                        //         device : deviceInfo                                                      
                        //     };
                        //     tmsService.getAgree(data).success(function (resp) {
                        //         $scope.res = resp;
                        //     }, function (err) {
                        //         $ionicPopup.alert({
                        //             title: '<i class="icon ion-minus-circled"></i> ERROR',
                        //             template: err
                        //         });
                        //     }).finally(function () {
                        //         $ionicPlatform.registerBackButtonAction(function (e) {
                        //             e.preventDefault();
                        //             $ionicHistory.goBack();
                        //             return false;
                        //         }, 100);
                        //         $scope.hide();
                        //     });
                        // }
                        
                        tmsService.doLogin(data).success(function (resp) {
                            if (resp.status == 1) {
                                $ionicHistory.clearHistory();
                                $ionicHistory.nextViewOptions({
                                    disableBack: true
                                });
                                // $scope.data.uname = "";
                                // $scope.data.passwd = "";
                                $scope.uid = resp.uid;
                                $scope.token = resp.token;
                                // $user = resp.username;
                                $window.localStorage['username'] = resp.username;
                                $window.localStorage['password'] = data.passwd;
                                $window.localStorage['name'] = resp.name;
                                $window.localStorage['uid'] = $scope.uid;
                                $window.localStorage['token'] = $scope.token;
                                $window.location.href = '#/main';
                                tmsService.doCheckTimesheet().success(function (resp) {
                                    if (resp.length > 0) {
                                        $scope.showAllert({
                                            title: '<i class="icon ion-minus-circled"></i> WARNING',
                                            message: 'Kamu mempunyai ' + resp.length + ' Timesheet yang belum di approve.'
                                        });

                                        try {
                                            cordova.plugins.notification.badge.requestPermission(function (granted) {
                                                if (granted)
                                                    cordova.plugins.notification.badge.set(resp.length);
                                                cordova.plugins.notification.badge.get(function (badge) {
                                                    console.log(badge);
                                                });
                                            });
                                        } catch (e) {
                                            console.log(e.message);
                                        }
                                    }
                                });
                                // console.log($window.localStorage);
                            } else if (resp.status == 3) {
                                $scope.showAllert({
                                    title: '<i class="icon ion-minus-circled"></i> CANNOT LOGIN',
                                    message: 'Ada driver yang sedang bertugas menggantikan anda.'
                                });
                            } else {
                                $scope.showAllert({
                                    title: '<i class="icon ion-minus-circled"></i> INVALID LOGIN',
                                    message: 'Please check your credentials!'
                                });
                            }
                        }, function (err) {
                            $scope.showAllert({
                                title: '<i class="icon ion-minus-circled"></i> ERROR',
                                message: err
                            });
                        }).finally(function () {
                            $scope.hide();
                        });
                    }
                };

                if (($window.localStorage['password'] != null) && ($window.localStorage['password'] != '') ) {
                    $scope.login();
                }
            } catch(ex) {
                console.log(ex.message);
            }
        })
        .controller('LogoutCtrl', function ($ionicHistory, $window) {
            $window.localStorage['uid'] = "1654364x564sdfsdfrtR?!@323#Sd%";
            $scope.data.uname = $window.localStorage['username'];
            $scope.data.passwd = '';
            $window.localStorage['password'] = '';
            // console.log($window.localStorage);
            // $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
        })
        .controller('MainCtrl', function ($scope, $ionicHistory, $ionicActionSheet, tmsService, $ionicLoading, $ionicPopup, $window) {
            $scope.uid = $window.localStorage['uid'];
            $scope.show = function () {
                $ionicLoading.show({
                    template: 'Mengambil data dari server...'
                });
            };
            $scope.hide = function () {
                $ionicLoading.hide();
            };
            
            $scope.mailto = function (email) {
                ionic.Platform.ready( function() {
                    window.open('mailto:'+email);
                });
            };
            
            $scope.show();
            $scope.unread = 0;
            var data = $window.localStorage['username'];
            tmsService.getUnreadMessage(data).success(function (response) {
                $scope.unread = response.unread;
                try {
                    cordova.plugins.notification.badge.requestPermission(function (granted) {
                        if (granted) {
                            cordova.plugins.notification.badge.get(function (badge) {
                                cordova.plugins.notification.badge.set(badge+response.unread);
                                console.log(badge+response.unread);
                            });
                        }
                    });
                } catch (e) {
                    console.log(e.message);
                }
            }, function (err) {
                $scope.unread = 0;
                $scope.hide();
            }).finally(function () {
                $scope.hide();
            });
            var id = 0;
            $scope.viewArticle = function (ids) {
                // if (ids != null)
                //     id = ids;
                // $window.location.href = "#/article/";
            };
            if ($scope.uid == "1654364x564sdfsdfrtR?!@323#Sd%" || $scope.uid == null) {
                // window.location = 'http://timesheet.mpm-rent.net/';
                window.location = server;
            } else {
                $scope.username = $window.localStorage['username'];
                $scope.name = $window.localStorage['name'];
                $scope.menus = [
                    {id: 'new', label: 'Absen Masuk', icon: 'compose'},
                    {id: 'list', label: 'Data Absen / Absen Pulang', icon: 'clock'},
                    {id: 'report', label: 'Rekap Absen', icon: 'pie-graph'},
                    // {id: 'bstklist', label: 'BSTK', icon: 'clipboard'},
                    // {id: 'editprofile/'+$scope.username, label: 'Profile', icon: 'clipboard'},
                    {id: 'message', label: 'Inbox', icon: 'android-textsms'},
                    // {id: 'email', label: 'Kirim Email', icon: 'android-send'},
                    // {id: 'article', label: 'News', icon: 'ios-paper'}
                ];
                $scope.doLogout = function () {
                    // $ionicHistory.clearCache();
                    // $scope.data.passwd = '';
                    $window.localStorage['password'] = null;
                    // console.log($window.localStorage);
                    $ionicHistory.clearHistory();
                    $ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
                    $window.localStorage['token'] = null;
                    $window.localStorage['uid'] = "1654364x564sdfsdfrtR?!@323#Sd%";
                    // $window.localStorage['username'] = 'aesdfawd';
                    // alert($window.localStorage['username']);
                    // window.location = 'http://timesheet.mpm-rent.net/';
                    // window.location = server;
                };
            }
        })
        .controller('FormCtrl', function ($scope, $ionicPlatform, tmsService, $ionicPopup, $ionicLoading, $window, $filter, Camera) {
            $scope.uid = $window.localStorage['uid'];
            // console(Device.uuid);
            
            function setCurrentPosition (position) {
                console.log(position);
                var geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                var request = {
                    latLng: latlng
                };
                geocoder.geocode(request, function(data, status) {
                    console.log(data);
                    var location = "";
                    var locationName = "";
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (data[0] != null) {
                            $window.localStorage['location'] = position.coords.latitude + ',' + position.coords.longitude + ',' + position.coords.accuracy;
                            $window.localStorage['locationName'] = data[0].formatted_address;
                        } else {
                            console.log(status);
                        }
                    } else {
                        console.log(status);
                    }
                });
            }
            function showGPSError (error) {
                tmsService.checkCountry().success(function (response) {
                    console.log(response);
                    $window.localStorage['location'] = response.lat + ',' + response.lon + ',0';
                    $window.localStorage['locationName'] = response.city  + ', ' + response.regionName  + ', ' + response.country  + ', timezone:' + response.timezone  + ', ' + response.query;
                });
                var content;
                switch (error.code) {
                    case error.PERMISSION_DENIED:
                        content = "User denied the request for Geolocation.";
                        break;
                    case error.POSITION_UNAVAILABLE:
                        content = "Location information is unavailable.";
                        break;
                    case error.TIMEOUT:
                        content = "The request to get user location timed out.";
                        break;
                    case error.UNKNOWN_ERROR:
                        content = "An unknown error occurred.";
                        break;
                }
                console.log(content);
            }
          navigator.geolocation.getCurrentPosition(setCurrentPosition, showGPSError, {
                enableHighAccuracy: true,
                timeout: 10000
            });
            // console.log($window.localStorage);
            // $scope.lastPhoto = 'assets/img/take_pic.png';
            // $scope.tms.pic = 'assets/img/take_pic.png';
            if ($scope.uid == "1654364x564sdfsdfrtR?!@323#Sd%" || $scope.uid == null) {
                // window.location = 'http://timesheet.mpm-rent.net/';
                window.location = server;
            }
            $scope.showAllert = function (msg) {
                $ionicPopup.alert({
                    title: msg.title,
                    template: msg.message,
                    okText: 'Ok',
                    okType: 'button-positive'
                });
            };
            var today = new Date(new Date().toUTCString().split('.')[0]);
            $scope.tms = {
                ot: 'no',
                driver_gs: false,
                used_for: 'internal',
                // used_for: 'office',
                province: $window.localStorage['province'],
                start_date: today,
                start_time: today,
                // end_date: today,
                // end_time: today
            };
            var data = {
                uname: $window.localStorage['username'],
            };
            tmsService.cekAgree(data).success(function (resp1){
                $scope.check = {};

                if (resp1 == false) {
                    $ionicPlatform.registerBackButtonAction(function (e) {
                        // agreeChecked();
                    }, 401);
                    
                    $ionicPopup.prompt({
                        template: '<ion-list class="list"> <ul style="text-align: justify; !important">'+
                        'Dengan ini menyatakan :<hr>'+
                        '<li>1. Bahwa saya telah membaca, mengerti dan memahami, Surat keputusan Direksi No : SKEP.001.03.2018.MMM.DIR, tentang Penggunaan Aplikasi Time Sheet Mobile (TSM) sebagai Absensi Harian Driver.<br></li>'+
                        '<li>2. Bahwa saya bersedia melakukan absensi harian melalui aplikasi TSM setiap hari dan memastikan absensi tersebut sudah disetujui oleh user.<br></li>'+
                        '<li>3. Bahwa apabila saya tidak mengisi TSM,saya bersedia menerima sanksi berupa penundaan pembayaran upah kerja lembur yang akan dibayarkan oleh perusahaan setelah saya 100% (seratus persen) mengisi absensi dengan TSM, dan /atau sanksi lain sesuai dengan peraturan Perusahaan yang berlaku.</li><br>'+
                        
                        '<ion-checkbox onClick="" id="backAgree" style="padding: 10px; padding-left: 60px;white-space: inherit;font-size: 15px;" ng-model="check.checkAgr"> <span id="textAgree" style="color: #f47e3d;white-space: normal;text-align: justify;">Demikianlah surat pernyataan ini saya buat dengan keadaan sadar dan tanpa paksaan dari pihak manapun.</span></ion-checkbox><br>'+
                        '</ul></ion-list>',
                        title: '<i class="icon ion-information-circled"></i> Pernyataan',
                        scope: $scope,
                        buttons: [
                            {
                                text: 'Ya',
                                type: 'button-positive',
                                onTap: function (e) {
                                    if (!$scope.check.checkAgr) {
                                        e.preventDefault();
                                        alertButton();
                                    } else {
                                        var data = {
                                            username: $window.localStorage['username'],
                                            device : deviceInfo                                                      
                                        };
                                        tmsService.getAgree(data).success(function (resp) {
                                            $scope.res = resp;
                                        }, function (err) {
                                            $ionicPopup.alert({
                                                title: '<i class="icon ion-minus-circled"></i> ERROR',
                                                template: err
                                            });
                                        }).finally(function () {
                                            $ionicPlatform.registerBackButtonAction(function (e) {
                                                e.preventDefault();
                                                $ionicHistory.goBack();
                                                return false;
                                            }, 100);
                                            $scope.hide();
                                        });
                                    }
                                }
                            }
                        ]
                    });
                }
            });
            // tmsService.getProvince().success(function (resp) {
            //     $scope.provinces = resp;
            // });
            
            var getDriver = 0;
            $scope.standbyChanged = function (e) {
                if ($scope.tms.standby) {
                    $scope.tms.code_driver = 'standby';
                } else {
                    $scope.tms.code_driver = '';
                }
            };
            $scope.gsChanged = function () {
                if ($scope.tms.driver_gs) {
                    $scope.show = function (message) {
                        $ionicLoading.show({
                            template: message
                        });
                    };
                    $scope.hide = function () {
                        $ionicLoading.hide();
                    };
                    $scope.show('Loading...');
                    if (getDriver == 0) {
                        tmsService.getDriver().success(function (resp) {
                            $scope.menus = resp;
                            getDriver = 1;
                            $scope.hide();

                            $ionicPopup.confirm({
                                title: '<i class="icon ion-person"></i> Driver GS',
                                scope: $scope,
                                template: '<ion-item class="item item-input"><span class="input-label">Standby</span>'+
                                '<ion-checkbox ng-model="tms.standby" ng-change="standbyChanged($event)"></ion-checkbox> '+
                                '</ion-item> '+
                                '<p style="margin-top: 10px;">atau masukan kode driver yang di gantikan.</p>'+
                                '<ion-item class="item item-input"><span class="input-label">Kode Driver</span>'+
                                '<input  type="text" ng-value="prop_new_val" ng-model="tms.code_driver">'+
                                '</ion-item> ',
                                buttons: [{
                                    text: 'Cancel',
                                    onTap: function(e) {
                                        return 'cancel'
                                    }
                                },
                                {
                                    text: 'Yes',
                                    onTap: function(e) {
                                        return $scope.tms.code_driver.toUpperCase();
                                    }
                                }]
                            }).then(function(res) {
                                if(res == 'cancel') {
                                    $scope.tms.driver_gs_code = '';
                                    elm.setAttribute('disabled', true);
                                    $scope.tms.driver_gs = false;
                                } else {
                                    $scope.tms.driver_gs_code = res;
                                }
                            });
                        });
                    } else {
                        $scope.hide();
                        $ionicPopup.confirm({
                            title: '<i class="icon ion-alert"></i> Form',
                            scope: $scope,
                            template: '<ion-item class="item item-input"><span class="input-label">Standby</span>'+
                            '<ion-checkbox ng-model="tms.standby" ng-change="standbyChanged($event)"></ion-checkbox> '+
                            '</ion-item> '+
                            '<p style="margin-top: 10px;">atau masukan kode driver yang di gantikan.</p>'+
                            '<ion-item class="item item-input"><span class="input-label">Kode Driver</span>'+
                            '<input  type="text" ng-value="prop_new_val" ng-model="tms.code_driver">'+
                            '</ion-item> ',
                            buttons: [{
                                text: 'Cancel',
                                onTap: function(e) {
                                    return 'cancel'
                                }
                            },
                            {
                                text: 'Yes',
                                onTap: function(e) {
                                    return $scope.tms.code_driver;
                                }
                            }]
                        }).then(function(res) {
                            if(res == 'cancel') {
                                $scope.tms.driver_gs_code = '';
                                elm.setAttribute('disabled', true);
                                $scope.tms.driver_gs = false;
                            } else {
                                $scope.tms.driver_gs_code = res;
                            }
                        });
                    }
                    var elm = document.querySelector("#driver_gs_code");
                    if($scope.tms.driver_gs) {
                        elm.removeAttribute('disabled');
                    } else {
                        elm.setAttribute('disabled', true);
                    }
                }
            };

            $scope.otChanged = function () {
                $scope.tms.town2 = '';
                var elm = document.querySelector("#town2");
                if ($scope.tms.ot != 'no') {
                    elm.removeAttribute('disabled');
                } else {
                    $scope.tms.town2 = '';
                    elm.setAttribute('disabled', true);
                }
            };

            $scope.overtimeChanged = function () {
                $ionicPopup.confirm({
                    title: '<i class="icon ion-alert"></i> ALERT',
                    template: 'Apakah anda yakin untuk lembur, ini membutuhkan persetujuan.',
                    buttons: [{
                        text: 'Cancel',
                        onTap: function(e) {return 'cancel'}
                    },
                    {
                        text: 'Yes'
                    }]
                }).then(function(res) {
                    if(res == 'cancel') {
                        console.log('Overtime cancel');
                        $scope.tms.overtime = false;
                    } else {
                        console.log('Overtime');
                        $scope.tms.overtime = true;
                    }
                });
            };
            $scope.addTimesheet = function () {
                $scope.fstart_date = $filter('date')($scope.tms.start_date, 'yyyy-MM-dd');
                $scope.fend_date = $filter('date')($scope.tms.end_date, 'yyyy-MM-dd');
                var dateInput = new Date(getFormattedDate($scope.tms.start_date) + ' ' + getFormattedTime($scope.tms.start_date));

                Date.prototype.addHours = function(h) {    
                    this.setTime(this.getTime() + (h*60*60*1000)); 
                    return this;   
                };

                if (!$scope.tms.start_date) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> ALERT',
                        message: 'Start date must not null'
                    });
                } else if ($scope.tms.start_time.getHours() < ((new Date().getHours()) - 0.25)) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Absen masuk tidak boleh lebih cepat dari waktu saat ini'
                    });
                } else if (!$scope.tms.start_time) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> ALERT',
                        message: 'Start time must not null'
                    });
                } else if (!$scope.tms.ot) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Mohon mengisi nama kota'
                    });
                } else if (!$scope.tms.nopol) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Mohon mengisi plat nomor'
                    });
                } else if (!$scope.tms.used_for) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Mohon mengisi data tugas'
                    });
                } else if ($scope.tms.start_date > $scope.tms.end_date) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Mohon cek kembali tanggal yang anda masukan'
                    });
                } else if (($scope.fstart_date == $scope.fend_date) && ($scope.tms.start_time > $scope.tms.end_time)) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Mohon cek kembali waktu yang anda masukan'
                    });
                } else if ((!$scope.tms.province) || ($scope.tms.province == '')) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Mohon pilih provinsi anda sedang bertugas.'
                    });
                // } else if ((!$scope.tms.nopol) && ($scope.tms.driver_gs_code != 'standby' )) {
                //     $scope.showAllert({
                //         title: '<i class="icon ion-alert"></i> PERINGATAN',
                //         message: 'Mohon masukan plat nomor kendaraan.'
                //     });
                } else if ((!$scope.tms.km) && ($scope.tms.driver_gs_code != 'standby' )) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Mohon masukan kilometer saat ini.'
                    });
                } else if ((('B'+$window.localStorage['username']).toUpperCase() == ('B'+$scope.tms.driver_gs_code).toUpperCase())) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Driver GS salah, mohon pilih kode driver yang digantikan.'
                    });
                } else if (($scope.tms.driver_gs) && ($scope.tms.driver_gs_code == null)) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Mohon pilih kode driver yang digantikan.'
                    });
                } else {
                    $scope.show = function () {
                        $ionicLoading.show({
                            template: 'Memproses data...'
                        });
                    };
                    $scope.hide = function () {
                        $ionicLoading.hide();
                    };
                    $scope.show();
                    var driver_gs_code = '';
                    var luar_kota = '';
                    var no_pol = '';
                    var appVersion = '';
                    $window.localStorage['province'] = $scope.tms.province;

                    if ($window.localStorage['version_app'] != null)
                        appVersion = $window.localStorage['version_app'];

                    if ($scope.tms.driver_gs)
                        driver_gs_code = $scope.tms.driver_gs_code; 

                    if ($scope.tms.town2)
                        luar_kota = 'Dinas luar kota ' + $scope.tms.town2;

                    if ($scope.tms.nopol)
                        no_pol = $scope.tms.nopol.toUpperCase().replace(/[^A-Z0-9]+/ig, "");
                    else
                        no_pol = '';
                    var data = {
                        start_date: $scope.tms.start_date,
                        start_time: $scope.tms.start_time,
                        overtime: $scope.tms.overtime,
                        // end_date: $scope.tms.end_date,
                        // end_time: $scope.tms.end_time,
                        location : $window.localStorage['location'],
                        location_name : $window.localStorage['locationName'],
                        ot: $scope.tms.ot,
                        town: $scope.tms.town,
                        town2: luar_kota,
                        province: $scope.tms.province,
                        nopol: no_pol,
                        // nopol: "",
                        km: $scope.tms.km,
                        driver_gs: driver_gs_code,
                        used_for: $scope.tms.used_for,
                        created_by: $window.localStorage['uid'],
                        uuid: $window.localStorage['uuid'],
                        model: $window.localStorage['model'],
                        manufacturer: $window.localStorage['manufacturer'],
                        version: $window.localStorage['version'],
                        platform: $window.localStorage['platform'],
                        version_app: appVersion
                        // picture: picdataURItoBlob($scope.tms.pic)
                    };
                    tmsService.add(data).success(function (resp) {
                        console.log(data);
                        if (resp.status == 1) {
                            $scope.showAllert({
                                title: '<i class="icon ion-checkmark-round"></i> SUCCESS',
                                message: 'Data berhasil diproses'
                            });
                        } else if (resp.status == 2) {
                            $scope.showAllert({
                                title: '<i class="icon ion-minus-circled"></i> ERROR',
                                message: 'Mohon untuk absen pulang dahulu sebelum anda absen masuk kembali !'
                            });
                        } else if (resp.status == 99) {
                            $scope.showAllert({
                                title: '<i class="icon ion-minus-circled"></i> ERROR',
                                message: resp.message
                            });
                        } else if (resp.status == 0) {
                            $scope.showAllert({
                                title: '<i class="icon ion-minus-circled"></i> ERROR',
                                message: resp.message
                            });
                        } else {
                            $scope.showAllert({
                                title: '<i class="icon ion-checkmark-round"></i> SUCCESS',
                                message: 'Data berhasil diproses'
                            });
                        }
                        $scope.tms.start_date = "";
                        $scope.tms.start_time = "";
                        // $scope.tms.end_date = "";
                        // $scope.tms.end_time = "";
                        $scope.tms.ot = "no";
                        $scope.tms.overtime = "0";
                        $scope.tms.town = "";
                        $scope.tms.town2 = "";
                        $scope.tms.driver_gs = false;
                        $scope.tms.province = '';
                        $scope.tms.driver_gs_code = '';
                        $scope.tms.nopol = "";
                        $scope.tms.km = "";
                        $scope.tms.used_for = "office";
                        // $scope.tms.pic = "";
                        // $scope.lastPhoto = 'assets/img/take_pic.png';
                        $window.location.href = '#/main';
                    }, function (err) {
                        $scope.showAllert({
                            title: '<i class="icon ion-minus-circled"></i> ERROR',
                            message: err
                        });
                        $scope.hide();
                        console.log(err);
                    }).finally(function () {
                        $scope.hide();
                    });
                }
            };
        })
        .controller('ReportCtrl', function ($scope, tmsService, $ionicLoading, $ionicPopup, $window) {
            $scope.uid = $window.localStorage['uid'];
            $scope.show = function () {
                $ionicLoading.show({
                    template: 'Mengambil data dari server...'
                });
            };
            $scope.hide = function () {
                $ionicLoading.hide();
            };
            $scope.show();
            $scope.tmss = [];
            tmsService.getAllReport().success(function (response) {
                $scope.tmss = response;
            }).finally(function () {
                $scope.hide();
            });
            $scope.doRefresh = function () {
                tmsService.getAllReport().success(function (response) {
                    $scope.tmss = response;
                }).finally(function () {
                    $scope.$broadcast('scroll.refreshComplete');
                });
            };
            $scope.viewDetails = function (id) {
                $window.location.href = "#/listdetail/" + id + "/yes";
            };
            $scope.searchReport = function () {
                $scope.datadate = {};
                $ionicPopup.prompt({
                    template: '<ion-list class="list"><ion-item class="item item-input"><span class="input-label">Dari Tanggal</span><input type="date" ng-model="datadate.from_date"></ion-item><ion-item class="item item-input"><span class="input-label">Sampai Tanggal</span><input type="date" ng-model="datadate.to_date"></ion-item></ion-list>',
                    title: '<i class="icon ion-information-circled"></i> MENCARI ABSEN',
                    scope: $scope,
                    buttons: [
                        {text: 'Batal'},
                        {
                            text: 'Mencari',
                            type: 'button-positive',
                            onTap: function () {
                                $ionicLoading.show({
                                    template: 'Mengambil data dari server...'
                                });
                                var datadate = {
                                    from: $scope.datadate.from_date,
                                    to: $scope.datadate.to_date,
                                    user_id: $window.localStorage['uid']                                                       
                                };
                                tmsService.searchReport(datadate).success(function (srcresp) {
                                    $scope.tmss = srcresp;
                                }, function (err) {
                                    $ionicPopup.alert({
                                        title: '<i class="icon ion-minus-circled"></i> ERROR',
                                        template: err
                                    });
                                }).finally(function () {
                                    $scope.hide();
                                });
                                console.log(datadate);
                            }
                        }
                    ]
                });
            };
        })
        .controller('ListCtrl', function ($scope, $ionicActionSheet, tmsService, $ionicLoading, $ionicPopup, $window) {
            $scope.uid = $window.localStorage['uid'];
            $scope.show = function () {
                $ionicLoading.show({
                    template: 'Mengambil data dari server...'
                });
            };
            $scope.hide = function () {
                $ionicLoading.hide();
            };
            $scope.show();
            $scope.tmss = [];
            tmsService.getAllTimesheet().success(function (response) {
                $scope.tmss = response;
            }).finally(function () {
                $scope.hide();
            });
            $scope.doRefresh = function () {
                tmsService.getAllTimesheet().success(function (response) {
                    $scope.tmss = response;
                }).finally(function () {
                    // Stop ion-refresh from spinning
                    $scope.$broadcast('scroll.refreshComplete');
                    $scope.checkIndexs = [];
                    $scope.timeIds = [];
                });
            };
            $scope.timeIds = [];
            $scope.checkIndexs = [];
            $scope.checkIndex = function (timeId, tms) {
                $scope.timeId = timeId;
                $scope.tms = tms;
                if ($scope.checkIndexs.indexOf($scope.tms) === -1) {
                    $scope.checkIndexs.push($scope.tms);
                    $scope.timeIds.push($scope.timeId);
                } else {
                    $scope.checkIndexs.splice($scope.checkIndexs.indexOf($scope.tms), 1);
                    $scope.timeIds.splice($scope.timeIds.indexOf($scope.timeId), 1);
                }
            };
            $scope.removeItem = function (index) {
                angular.forEach($scope.checkIndexs, function (value, index) {
                    var index = $scope.tmss.indexOf(value);
                    $scope.tmss.splice($scope.tmss.indexOf(value), 1);
                });
                $scope.checkIndexs = [];
                $scope.timeIds = [];
            };
            $scope.showOption = function (id, tms) {
                $scope.id = id;
                $scope.tms = tms;
                $ionicActionSheet.show({
                    titleText: 'Pilihan',
                    buttons: [
                        {text: 'Absen Pulang / チェックアウト'},
                        {text: 'Melihat detail / 詳細は不在です'},
                        {text: 'Menyetujui / 承認する場合、'},
                        // {text: 'Mencari / 検索'},
                    ],
                    destructiveText: 'Hapus / 削除',
                    cancelText: 'batal',
                    cancel: function () {
                    },
                    buttonClicked: function (index) {
                        if (index == 2) {
                            if ($scope.timeIds.length == 0) {
                                $ionicPopup.alert({
                                    title: '<i class="icon ion-alert"></i> PERINGATAN',
                                    template: 'Mohon untuk memilih absen dahulu'
                                });
                            } else {
                                var data = {
                                    uid: $window.localStorage['uid']
                                };
                                tmsService.getPICApprove(data).success(function (response) {
                                    if (response.status == 1) {
                                        $scope.datapin = {};
                                            // '<input type="radio" ng-model="datapin.radio" value="6" name="radio" id="radio3" />'+
                                            // '<label for="radio3" class="rad">'+
                                            // '<img src="assets/img/3.png">'+
                                            // '</label>'+
                                            // <br>あなたが受け取ったサービスに価値を与える
                                        $ionicPopup.prompt({
                                            template: '<span class="">Pin anda sangat rahasia, harap berhati-hati.<br>PINの入力をお願いします</span><input class="input-pin" placeholder="Masukan PIN anda / PINの入力" type="password" ng-model="datapin.pin">'+
                                            '<br><hr>Berikan nilai untuk service yang Anda terima<br>本日の運転手サービスはいかがでしたか。<hr>'+'<div style="text-align: center;" class="radio-toolbar">'+
                                            '<input onClick="radioChecked(1)" type="radio" ng-model="datapin.radio" value="2" name="radio" id="radio1"/>'+
                                            '<label for="radio1" class="rad">'+
                                            '<img id="radioImg1" src="assets/img/Bintang-Abu.png">'+
                                            '</label>'+
                                            '<input onClick="radioChecked(2)" type="radio" ng-model="datapin.radio" value="4" name="radio" id="radio2"/>'+
                                            '<label for="radio2" class="rad">'+
                                            '<img id="radioImg2" src="assets/img/Bintang-Abu.png">'+
                                            '</label>'+
                                            '<input onClick="radioChecked(3)" type="radio" ng-model="datapin.radio" value="6" name="radio" id="radio3"/>'+
                                            '<label for="radio3" class="rad">'+
                                            '<img id="radioImg3" src="assets/img/Bintang-Abu.png">'+
                                            '</label>'+
                                            '<input onClick="radioChecked(4)" type="radio" ng-model="datapin.radio" value="8" name="radio" id="radio4"/>'+
                                            '<label for="radio4" class="rad">'+
                                            '<img id="radioImg4" src="assets/img/Bintang-Abu.png">'+
                                            '</label>'+
                                            '</div>',
                                            title: '<i class="icon ion-alert"></i> KONFIRMASI Mr/Ms. '+ response.name + '<br><p>名前の確認 Mr/Ms. '+ response.name + '</p>',
                                            scope: $scope,
                                            buttons: [
                                                {text: 'Batal<br>承認する場合、'},
                                                {
                                                    text: 'Menyetujui<br>承認する場合、',
                                                    type: 'button-positive',
                                                    onTap: function (e) {
                                                        if ((!$scope.datapin.pin) || (!$scope.datapin.radio)) {
                                                            e.preventDefault();
                                                        } else {
                                                            $ionicLoading.show({
                                                                template: 'Verifying...'
                                                            });
                                                            var data = {
                                                                uid: $window.localStorage['uid'],
                                                                pwd: $scope.datapin.pin,
                                                                survey: $scope.datapin.radio,
                                                                ids: $scope.timeIds
                                                            };
                                                            tmsService.approve(data).success(function (response) {
                                                                if (response.status == 0) {
                                                                    $ionicPopup.alert({
                                                                        title: '<i class="icon ion-minus-circled"></i> ERROR',
                                                                        template: 'Mohon periksa kembali data yang dimasukan!'
                                                                    });
                                                                } else if (response.status == 1) {
                                                                    $ionicPopup.alert({
                                                                        title: '<i class="icon ion-minus-circled"></i> ERROR',
                                                                        template: 'Kamu tidak mempunyai akses untuk menyetujui absen ini'
                                                                    });
                                                                } else if (response.status == 7) {
                                                                    $ionicPopup.alert({
                                                                        title: '<i class="icon ion-minus-circled"></i> ERROR',
                                                                        template: 'Kamu tidak mempunyai akses untuk menyetujui absen ini (Driver GS).'
                                                                    });
                                                                } else if (response.status == 6) {
                                                                    $ionicPopup.alert({
                                                                        title: '<i class="icon ion-minus-circled"></i> ERROR',
                                                                        template: 'Kamu tidak mempunyai akses untuk menyetujui absen ini (Driver GS).'
                                                                    });
                                                                } else if (response.status == 5) {
                                                                    $ionicPopup.alert({
                                                                        title: '<i class="icon ion-minus-circled"></i> ERROR',
                                                                        template: 'Kamu belum absen pulang.'
                                                                    });
                                                                } else if (response.status == 3) {
                                                                    $ionicPopup.alert({
                                                                        title: '<i class="icon ion-checkmark-round"></i> SUCCESS / 完了',
                                                                        template: 'Absen berhasil disetujui<br>承認完了.'
                                                                    });
                                                                    $scope.removeItem();
                                                                }
                                                            }, function (err) {
                                                                $ionicPopup.alert({
                                                                    title: '<i class="icon ion-minus-circled"></i> ERROR',
                                                                    template: err
                                                                });
                                                            }).finally(function () {
                                                                $scope.hide();
                                                            });
                                                        }
                                                    }
                                                }
                                            ]
                                        });
                                    } else {
                                        $ionicPopup.alert({
                                            title: '<i class="icon ion-alert"></i> ALERT',
                                            template: 'Mohon untuk menghubungi administrator.'
                                        });
                                    }
                                });
                            }
                            return true;
                        } else if (index == 1) {
                            if ($scope.timeIds.length == 0) {
                                $ionicPopup.alert({
                                    title: '<i class="icon ion-alert"></i> WARNING',
                                    template: 'Mohon untuk memilih absen'
                                });
                            } else if ($scope.timeIds.length == 1) {
                                $scope.tid = $scope.timeIds[0];
                                $window.location.href = "#/listdetail/" + $scope.tid + "/no";
                            } else {
                                $ionicPopup.alert({
                                    title: '<i class="icon ion-alert"></i> WARNING',
                                    template: 'Mohon untuk memilih hanya 1 absen'
                                });
                            }
                            return true;
                        } else if (index == 3) {

                            $scope.datadate = {};
                            $ionicPopup.prompt({
                                template: '<ion-list class="list"><ion-item class="item item-input"><span class="input-label">Dari Tanggal</span><input type="date" ng-model="datadate.from_date"></ion-item><ion-item class="item item-input"><span class="input-label">Sampai Tanggal</span><input type="date" ng-model="datadate.to_date"></ion-item></ion-list>',
                                title: '<i class="icon ion-search"></i> MENCARI ABSEN',
                                scope: $scope,
                                buttons: [
                                    {text: 'Batal'},
                                    {
                                        text: 'Mencari',
                                        type: 'button-positive',
                                        onTap: function () {
                                            $ionicLoading.show({
                                                template: 'Mengambil data dari server...'
                                            });
                                            var datadate = {
                                                from: $scope.datadate.from_date,
                                                to: $scope.datadate.to_date,
                                                user_id: $window.localStorage['uid']
                                            };
                                            tmsService.searchTimesheet(datadate).success(function (srcresp) {
                                                $scope.tmss = srcresp;
                                            }, function (err) {
                                                $ionicPopup.alert({
                                                    title: '<i class="icon ion-minus-circled"></i> ERROR',
                                                    template: err
                                                });
                                            }).finally(function () {
                                                $scope.hide();
                                            });
                                            console.log(datadate);
                                        }
                                    }
                                ]
                            });
                            return true;

                        } else {
                            if ($scope.timeIds.length == 0) {
                                $ionicPopup.alert({
                                    title: '<i class="icon ion-alert"></i> PERINGATAN',
                                    template: 'Mohon untuk memilih waktu yang benar'
                                });
                            } else if ($scope.timeIds.length == 1) {
                                $scope.tid = $scope.timeIds[0];
                                $window.location.href = "#/formedit/" + $scope.tid;
                            } else {
                                $ionicPopup.alert({
                                    title: '<i class="icon ion-alert"></i> PERINGATAN',
                                    template: 'Mohon untuk memilih waktu yang benar'
                                });
                            }
                            return true;
                        }
                    },
                    destructiveButtonClicked: function () {
                        if ($scope.timeIds.length == 0) {
                            $ionicPopup.alert({
                                title: '<i class="icon ion-alert"></i> WARNING',
                                template: 'Mohon untuk memilih waktu yang benar'
                            });
                        } else {
                            $ionicPopup.prompt({
                                template: '<ion-list class="list"></ion-list>',
                                title: '<i class="icon ion-close"></i> MENGHAPUS ABSEN',
                                scope: $scope,
                                buttons: [
                                    {text: 'Batal'},
                                    {
                                        text: 'Ok',
                                        type: 'button-positive',
                                        onTap: function () {
                                            $ionicLoading.show({
                                                template: 'Menghapus...'
                                            });
                                            var data = {
                                                id: $scope.timeIds
                                            };
                                            tmsService.deleteTime(data).success(function (response) {
                                                if (response.status == 1) {
                                                    $ionicLoading.hide();
                                                    $scope.removeItem();
                                                    //console.log(response);
                                                } else {
                                                    $ionicLoading.hide();
                                                }
                                            }, function (err) {
                                                $ionicPopup.alert({
                                                    title: '<i class="icon ion-alert"></i> KESALAHAN',
                                                    template: err
                                                });
                                            }).finally(function () {
                                                $ionicLoading.hide();
                                            });
                                        }
                                    }
                                ]
                            });
                        }
                        return true;
                    }
                });
            };
        })
        .controller('ListDetailCtrl', function ($scope, tmsService, $stateParams, $ionicLoading) {
            $scope.show = function () {
                $ionicLoading.show({
                    template: 'Mengambil data dari server...'
                });
            };
            $scope.hide = function () {
                $ionicLoading.hide();
            };
            $scope.show();
            $scope.timeId = $stateParams.timeId;
            tmsService.getIdTimesheet($scope.timeId).success(function (response) {
                $scope.tmssdetails = response;
            }).finally(function () {
                $scope.hide();
            });
            $scope.status = $stateParams.status;
            if ($scope.status === "no") {
                $scope.statusLabel = "BELUM DISETUJUI";
                $scope.color = "red";
            } else {
                $scope.statusLabel = "DISETUJUI";
                $scope.color = "green";
            }
        })

        .controller('FormEditCtrl', function ($scope, tmsService, $stateParams, $ionicLoading, $ionicPopup, $filter, $window) {
            $scope.uid = $window.localStorage['uid'];
            function setCurrentPosition (position) {
                console.log(position);
                var geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                var request = {
                    latLng: latlng
                };
                geocoder.geocode(request, function(data, status) {
                    console.log(data);
                    var location = "";
                    var locationName = "";
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (data[0] != null) {
                            $window.localStorage['location'] = position.coords.latitude + ',' + position.coords.longitude + ',' + position.coords.accuracy;
                            $window.localStorage['locationName'] = data[0].formatted_address;
                        } else {
                            console.log(status);
                        }
                    } else {
                        console.log(status);
                    }
                });
            }
            function showGPSError (error) {
                tmsService.checkCountry().success(function (response) {
                    console.log(response);
                    $window.localStorage['location'] = response.lat + ',' + response.lon + ',0';
                    $window.localStorage['locationName'] = response.city  + ', ' + response.regionName  + ', ' + response.country  + ', timezone:' + response.timezone  + ', ' + response.query;
                });
                var content;
                switch (error.code) {
                    case error.PERMISSION_DENIED:
                        content = "User denied the request for Geolocation.";
                        break;
                    case error.POSITION_UNAVAILABLE:
                        content = "Location information is unavailable.";
                        break;
                    case error.TIMEOUT:
                        content = "The request to get user location timed out.";
                        break;
                    case error.UNKNOWN_ERROR:
                        content = "An unknown error occurred.";
                        break;
                }
                console.log(content);
            }
          navigator.geolocation.getCurrentPosition(setCurrentPosition, showGPSError, {
                enableHighAccuracy: true,
                timeout: 10000
            });

            $scope.otChanged = function () {
                $scope.tms.town2 = '';
                var elm = document.querySelector("#town2");
                if ($scope.tms.ot != 'no') {
                    elm.removeAttribute('disabled');
                } else {
                    $scope.tms.town2 = '';
                    elm.setAttribute('disabled', true);
                }
            };
            if ($scope.uid == "1654364x564sdfsdfrtR?!@323#Sd%" || $scope.uid == null) {
                // window.location = 'http://timesheet.mpm-rent.net/';
                window.location = server;
            }
            $scope.showAllert = function (msg) {
                $ionicPopup.alert({
                    title: msg.title,
                    template: msg.message,
                    okText: 'Ok',
                    okType: 'button-positive'
                });
            };
            $scope.show = function () {
                $ionicLoading.show({
                    template: 'Mengambil data dari server...'
                });
            };

            $scope.hide = function () {
                $ionicLoading.hide();
            };
            $scope.show();
            $scope.timeId = $stateParams.timeId;
            tmsService.getIdTimesheet($scope.timeId).success(function (response) {
                $scope.tmssdetails = response;

                var arr = $scope.tmssdetails[0].start_time.split(/[- :]/),
                date = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
            // alert(new Date(date));
                var today = new Date(new Date().toUTCString().split('.')[0]);
                var driver_gs = false;
                if ($scope.tmssdetails[0].driver_gs != '') 
                    driver_gs = true;
                if ($scope.tmssdetails[0].end_time != null) 
                    today = new Date($scope.tmssdetails[0].end_time);
                $scope.tms = {
                    id: $scope.tmssdetails[0].id,
                    start_date: date,
                    start_time: date,
                    end_date: today,
                    end_time: today,
                    end_date_edit: new Date($scope.tmssdetails[0].start_time_edit),
                    end_time_edit: new Date($scope.tmssdetails[0].start_time_edit),
                    province: $scope.tmssdetails[0].province,
                    ot: $scope.tmssdetails[0].out_of_town,
                    town: $scope.tmssdetails[0].town,
                    driver_gs: $scope.tmssdetails[0].driver_gs,
                    driver_gs_code: $scope.tmssdetails[0].driver_gs,
                    used_for: $scope.tmssdetails[0].type_of_use,
                    town2: $scope.tmssdetails[0].desc
                };
            }).finally(function () {
                $scope.hide();
            });
            $scope.updateTimesheet = function () {
                var dateInput = new Date(getFormattedDate($scope.tms.end_date) + ' ' + getFormattedTime($scope.tms.end_time));
                // alert( dateInput.getTime()  + ' - ' + new Date().getTime() );
                if (!$scope.tms.start_date) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Tanggal absen masuk tidak boleh kosong'
                    });
                } else if (!$scope.tms.start_time) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Waktu absen masuk tidak boleh kosong'
                    });
                } else if (!$scope.tms.end_date) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Tanggal absen pulang tidak boleh kosong'
                    });
                } else if (!$scope.tms.end_time) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Waktu absen pulang tidak boleh kosong'
                    });
                } else if (dateInput.getTime() > (new Date().getTime())) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Absen pulang tidak boleh lebih dari waktu saat ini'
                    });
                } else if (!$scope.tms.ot) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Mohon untuk memasukan nama kota'
                    });
                } else if ((!$scope.tms.used_for) && ($scope.tms.driver_gs_code != 'standby' )) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Mohon mengisi data tugas'
                    });
                } else if ((!$scope.tms.km) && ($scope.tms.driver_gs_code != 'standby' )) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> PERINGATAN',
                        message: 'Mohon masukan kilometer saat ini'
                    });
                // } else if (!$scope.tms.desc) {
                //     $scope.showAllert({
                //         title: '<i class="icon ion-alert"></i> PERINGATAN',
                //         message: 'Mohon masukan keterangan'
                //     });
                } else {
                    $scope.show = function () {
                        $ionicLoading.show({
                            template: 'Merubah data...'
                        });
                    };
                    $scope.hide = function () {
                        $ionicLoading.hide();
                    };
                    $scope.show();
                    
                    var data = {
                        id: $scope.timeId,
                        start_date: $scope.tms.start_date,
                        start_time: $scope.tms.start_time,
                        end_date: $scope.tms.end_date,
                        end_time: $scope.tms.end_time,
                        ot: $scope.tms.ot,
                        province: $scope.tms.province,
                        town: $scope.tms.town,
                        town2: $scope.tms.town2,
                        km: $scope.tms.km,
                        desc: $scope.tms.desc,
                        driver_gs: $scope.tms.driver_gs_code,
                        used_for: $scope.tms.used_for,
                        location_end: $window.localStorage['location'],
                        location_name_end: $window.localStorage['locationName']
                    };
                    tmsService.update(data).success(function (resp) {
                        console.log(data);
                        $scope.showAllert({
                            title: '<i class="icon ion-checkmark-round"></i> SUCCESS',
                            message: 'Data berhasil dirubah'
                        });
                        $scope.start_date = "";
                        $scope.start_time = "";
                        $scope.end_date = "";
                        $scope.end_time = "";
                        $scope.ot = "";
                        $scope.town = "";
                        $scope.driver_gs = false;
                        $scope.driver_gs_code = "";
                        $scope.used_for = "";
                        $window.location.href = "#/list";
                    }, function (err) {
                        $scope.showAllert({
                            title: '<i class="icon ion-minus-circled"></i> ERROR',
                            message: err
                        });
                    }).finally(function () {
                        $scope.hide();
                    });
                }
            };
        })
        .controller('ArticleCtrl', function ($scope, $ionicActionSheet, tmsService, $ionicLoading, $ionicPopup, $window) {
            $scope.show = function () {
                $ionicLoading.show({
                    template: 'Mengambil data dari server...'
                });
            };
            $scope.hide = function () {
                $ionicLoading.hide();
            };
            $scope.show();
            tmsService.getArticles().success(function (response) {
                $scope.posts = response.posts;
                $scope.categories = response.categories;
            }).finally(function () {
                $scope.hide();
            });
            
            $scope.tabClick = function (idTab) {
                var all = document.getElementsByClassName("tab-content");
                for (var i=0, max=all.length; i < max; i++) {
                    all[i].style.display = 'none';
                }
                var allBtn = document.getElementsByClassName("tab-item");
                for (var i=0, max=all.length; i < max; i++) {
                    try {
                        allBtn[i].setAttribute("class", "tab-item");
                    }catch(err) {
                        
                    }
                    try {
                        var btn = document.getElementById("btnCat"+idTab);
                        btn.setAttribute("class", "tab-item active");
                    }catch(err) {
                        
                    }
                }
                if (idTab != 'More') {
                    var elm = document.getElementById("tab"+idTab);
                    elm.style.display = 'block';
                } else {
                    document.getElementById("tab18").style.display = 'block';
                    document.getElementById("tab20").style.display = 'block';
                    document.getElementById("tab21").style.display = 'block';
                }
                // btn.setAttribute("class", "tab-item active");
            };
            $scope.doRefresh = function () {
                tmsService.getArticles().success(function (response) {
                    $scope.posts = response.posts;
                    $scope.categories = response.categories;
                }).finally(function () {
                    $scope.$broadcast('scroll.refreshComplete');
                });
            };

            $scope.viewArticle = function (id) {
                $window.location.href = "#/articledetail/" + id;
            };
        })
        .controller('ArticleDetailCtrl', function ($scope, tmsService, $stateParams, $ionicLoading) {
            $scope.renderAsHtml = function(html)	{
                return $sce.trustAsHtml(html);
            };
            $scope.show = function () {
                $ionicLoading.show({
                    template: 'Mengambil data dari server...'
                });
            };
            $scope.hide = function () {
                $ionicLoading.hide();
            };
            
            $scope.show();
            tmsService.getArticleDetail($stateParams.idArticle).success(function (response) {
                $scope.articleDetail = response;
            }).finally(function () {
                $scope.hide();
            });
        })
        .controller('MessageCtrl', function ($scope, $ionicActionSheet, tmsService, $ionicLoading, $ionicPopup, $window) {
            $scope.show = function () {
                $ionicLoading.show({
                    template: 'Mengambil data dari server...'
                });
            };
            $scope.hide = function () {
                $ionicLoading.hide();
            };
            $scope.show();

            var data = $window.localStorage['username'];
            tmsService.getAllMessage(data).success(function (response) {
                $scope.messages = response.messages;
                // $scope.title = response.title;
                // $scope.text = response.text;
                // $scope.create_by = response.create_by;
                // $scope.datetime = response.datetime;
            }).finally(function () {
                $scope.hide();
            });

            $scope.doRefresh = function () {
                tmsService.getAllMessage(data).success(function (response) {
                    $scope.messages = response.messages;
                    // $scope.title = response.title;
                    // $scope.text = response.text;
                    // $scope.create_by = response.create_by;
                    // $scope.datetime = response.datetime;
                }).finally(function () {
                    $scope.$broadcast('scroll.refreshComplete');
                });
            };

            $scope.viewMessage = function (id) {
                $window.location.href = "#/messagedetail/" + id;
            };
        })
        .controller('MessageDetailCtrl', function ($scope, tmsService, $stateParams, $ionicLoading, $window) {
            // $scope.renderAsHtml = function(html)	{
            //     return $sce.trustAsHtml(html);
            // }
            $scope.show = function () {
                $ionicLoading.show({
                    template: 'Mengambil data dari server...'
                });
            };
            $scope.hide = function () {
                $ionicLoading.hide();
            };
            
            $scope.show();

            var data = {
                user : $window.localStorage['username'],
                id : $stateParams.idMessage
            };
            tmsService.getMessageDetail(data).success(function (response) {
                $scope.id = response.id;
                $scope.title = response.title;
                $scope.text = response.text;
                $scope.create_by = response.create_by;
                $scope.datetime = response.datetime;
            }).finally(function () {
                $scope.hide();
            });
        }).controller('BstkListCtrl', function ($scope, $ionicActionSheet, tmsService, $ionicLoading, $ionicPopup, $window) {
            $scope.uid = $window.localStorage['uid'];
            $scope.show = function () {
                $ionicLoading.show({
                    template: 'Mengambil data dari server...'
                });
            };
            $scope.hide = function () {
                $ionicLoading.hide();
            };
            $scope.show();
            $scope.insp = [];
            tmsService.getAllInsp().success(function (response) {
                $scope.insp = response;
            }).finally(function () {
                $scope.hide();
            });
            $scope.doRefresh = function () {
                tmsService.getAllInsp().success(function (response) {
                    $scope.insp = response;
                }).finally(function () {
                    // Stop ion-refresh from spinning
                    $scope.$broadcast('scroll.refreshComplete');
                    $scope.checkIndexs = [];
                    $scope.timeIds = [];
                });
            };
            $scope.timeIds = [];
            $scope.checkIndexs = [];
            $scope.checkIndex = function (timeId, tms) {
                $scope.timeId = timeId;
                $scope.tms = tms;
                if ($scope.checkIndexs.indexOf($scope.tms) === -1) {
                    $scope.checkIndexs.push($scope.tms);
                    $scope.timeIds.push($scope.timeId);
                } else {
                    $scope.checkIndexs.splice($scope.checkIndexs.indexOf($scope.tms), 1);
                    $scope.timeIds.splice($scope.timeIds.indexOf($scope.timeId), 1);
                }
            };
            $scope.showOption = function (id, tms) {
                $scope.id = id;
                $scope.tms = tms;
                $ionicActionSheet.show({
                    titleText: 'Pilihan',
                    buttons: [
                        {text: 'Membuat BSTK'},
                        // {text: 'Melihat detail'},
                        // {text: 'Menyetujui / 承認'},
                        // {text: 'Mencari / 検索'},
                    ],
                    // destructiveText: 'Hapus / 削除',
                    cancelText: 'Batal',
                    cancel: function () {
                    },
                    buttonClicked: function (index) {
                        if (index == 0) {  
                            $scope.data = {};
                            $ionicPopup.prompt({
                                template: '<span class="">Masukan nomor transaksi yang akan dibuatkan BSTK.</span>'+
                                '<input class="input-pin" placeholder="Masukan Nomor Transaksi" type="no_trans" ng-model="data.no_trans">',
                                title: '<i class="icon ion-alert"> Membuat BSTK</i>',
                                scope: $scope,
                                buttons: [
                                    {text: 'Batal'},
                                    {
                                        text: 'OK',
                                        type: 'button-positive',
                                        onTap: function (e) {
                                            if ((!$scope.data.no_trans)) {
                                                e.preventDefault();
                                            } else {
                                                $ionicLoading.show({
                                                    template: 'Loading...'
                                                });

                                                var data = {};
                                                tmsService.approve(data).success(function (response) {
                                                    $window.location.href = "#/bstk";
                                                }, function (err) {
                                                    $ionicPopup.alert({
                                                        title: '<i class="icon ion-minus-circled"></i> ERROR',
                                                        template: err
                                                    });
                                                }).finally(function () {
                                                    $scope.hide();
                                                });
                                            }
                                        }
                                    }
                                ]
                            });
                            return true;
                        } 
                    }
                });
            };

            $scope.viewBSTK = function (id) {
                $window.location.href = "#/detailbstk/" + id;
            };
        })
        .controller('BSTKCtrl', function ($scope, tmsService, $ionicPopup, $ionicLoading, $window, $filter, Camera) {
            $scope.uid = $window.localStorage['uid'];
            $scope.view = 'false';

            $scope.groups = [];
            tmsService.getChecklist().success(function (response) {
                var i = 0;
                var tes = [];

                var head = '';
                response.forEach(function(entry) {
                    if (head != entry.CATEGORYGROUPID) {
                        $scope.groups[i] = {
                            name: entry.CATEGORYGROUPID,
                            items: []
                        };
                        response.forEach(function(entry1) {
                            if (entry.CATEGORYGROUPID == entry1.CATEGORYGROUPID) {
                                $scope.groups[i].items.push(entry1.DESCRIPTION);
                            }
                        });
                        head = entry.CATEGORYGROUPID;
                        console.log($scope.groups[i]);
                        i++;
                    }
                });
            });

            $scope.toggleGroup = function(group) {
                if ($scope.isGroupShown(group)) {
                    $scope.shownGroup = null;
                } else {
                    $scope.shownGroup = group;
                    document.body.scrollBottom = 0; // For Safari
                    document.documentElement.scrollBottom = 0;
                }
            };
            
            $scope.isGroupShown = function(group) {
                return $scope.shownGroup === group;
            };
            
            $scope.showAllert = function (msg) {
                $ionicPopup.alert({
                    title: msg.title,
                    template: msg.message,
                    okText: 'Ok',
                    okType: 'button-positive'
                });
            };
            
        })
        .controller('DetailBSTKCtrl', function ($scope, tmsService, $stateParams, $ionicPopup, $ionicLoading, $window, $filter, Camera) {
            $scope.uid = $window.localStorage['uid'];
            $scope.view = 'true';
            $scope.bstb = [];

            $scope.show = function () {
                $ionicLoading.show({
                    template: 'Mengambil data dari server...'
                });
            };
            $scope.hide = function () {
                $ionicLoading.hide();
            };
            
            $scope.show();

            var data = {
                id : $stateParams.idBSTK
            };
            tmsService.getBSTKDetail(data).success(function (response) {
                $scope.groups = [];
                $scope.values = [];
                $scope.checked = [];
                $scope.header = response['header'];
                $scope.bstb.merk = $scope.header['RENTDEVICEGROUPID'] + ' | ' + $scope.header['DESCRIPTION'];
                $scope.bstb.tahun = $scope.header['ITK_MODELYEAR'];
                $scope.bstb.warna = $scope.header['DESCRIPTION'];
                $scope.bstb.lokasi = $scope.header['DESCRIPTION'];
                $scope.bstb.jenis = $scope.header['RENTDEVICEGROUPID'];
                $scope.bstb.license = $scope.header['ITK_REGISTRATIONNUMBER'];
                $scope.bstb.perusahaan = $scope.header['DESCRIPTION'];
                $scope.bstb.nama = $scope.header['DESCRIPTION'];
                $scope.bstb.alamat = $scope.header['DESCRIPTION'];
                
                var i = 0;
                var head = '';
                response['body'].forEach(function(entry) {
                    // console.log(entry);
                    if (head != entry.CATEGORYGROUPID) {
                        $scope.groups[i] = {
                            name: entry.CATEGORYGROUPID,
                            items: []
                        };
                        response['body'].forEach(function(entry1) {
                            if (entry.CATEGORYGROUPID == entry1.CATEGORYGROUPID) {
                                $scope.groups[i].items.push(entry1.DESCRIPTION);
                                if (entry1.CHECKED == 1) {
                                    $scope.values[entry1.DESCRIPTION] = 1;
                                    $scope.checked[entry1.DESCRIPTION] = 'true';
                                } else {
                                    $scope.values[entry1.DESCRIPTION] = 0;
                                    $scope.checked[entry1.DESCRIPTION] = 'false';
                                }
                            }
                        });
                        head = entry.CATEGORYGROUPID;
                        i++;
                    }
                });
            }).finally(function () {
                $scope.hide();
            });


            $scope.toggleGroup = function(group) {
                if ($scope.isGroupShown(group)) {
                    $scope.shownGroup = null;
                } else {
                    $scope.shownGroup = group;
                    document.body.scrollBottom = 0; // For Safari
                    document.documentElement.scrollBottom = 0;
                }
            };
            
            $scope.isGroupShown = function(group) {
                return $scope.shownGroup === group;
            };
            

            
            $scope.showAllert = function (msg) {
                $ionicPopup.alert({
                    title: msg.title,
                    template: msg.message,
                    okText: 'Ok',
                    okType: 'button-positive'
                });
            };
            
        }).controller('ProfileCtrl', function ($scope, $stateParams, tmsService, $ionicPopup, $ionicLoading, $window, $filter, Camera) {
            $scope.uid = $window.localStorage['uid'];

            $scope.showAllert = function (msg) {
                $ionicPopup.alert({
                    title: msg.title,
                    template: msg.message,
                    okText: 'Ok',
                    okType: 'button-positive'
                });
            };

            $scope.tms = {};
            tmsService.getDataDriver($stateParams.id).success(function (response) {
                $scope.tms = {
                    nama: response.firstname + ' ' + response.lastname,
                    email: response.email,
                    phone1: response.phone1,
                    phone2: response.phone2,
                    phone3: response.phone3,
                    kerabat: response.kerabat,
                    pic: response.pic
                };
            }).finally(function () {
                $scope.hide();
            });

            $scope.updateDataDriver = function () {
                if (!$scope.tms.email) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> ALERT',
                        message: 'Masukan alamat email dahulu.'
                    });
                } else if (!$scope.tms.phone1) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> ALERT',
                        message: 'Masukan nomor telepon 1 dahulu.'
                    });
                } else if (!$scope.tms.phone2) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> ALERT',
                        message: 'Masukan nomor telepon 2 dahulu.'
                    });
                } else if (!$scope.tms.phone3) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> ALERT',
                        message: 'Masukan nomor telepon kerabat dahulu.'
                    });
                } else if (!$scope.tms.kerabat) {
                    $scope.showAllert({
                        title: '<i class="icon ion-alert"></i> ALERT',
                        message: 'Masukan hubungan dengan kerabat dahulu.'
                    });
                } else {
                    $scope.show = function () {
                        $ionicLoading.show({
                            template: 'Memproses data...'
                        });
                    };
                    $scope.hide = function () {
                        $ionicLoading.hide();
                    };
                    $scope.show();
                    var data = {
                        id: $stateParams.id,
                        email: $scope.tms.email,
                        phone1: $scope.tms.phone1,
                        phone2 : $scope.tms.phone2,
                        phone3 : $scope.tms.phone3,
                        kerabat: $scope.tms.kerabat,
                        picture: ''
                        // picture: picdataURItoBlob($scope.tms.pic)
                    };
                    tmsService.updateDataDriver(data).success(function (resp) {
                        console.log(data);
                        if (resp.status == 1) {
                            $scope.showAllert({
                                title: '<i class="icon ion-checkmark-round"></i> SUCCESS',
                                message: 'Data pengemudi berhasil diubah.'
                            });
                        } else if (resp.status == 0) {
                            $scope.showAllert({
                                title: '<i class="icon ion-minus-circled"></i> ERROR',
                                message: resp.message
                            });
                        } 
                        $window.location.href = '#/main';
                    }, function (err) {
                        $scope.showAllert({
                            title: '<i class="icon ion-minus-circled"></i> ERROR',
                            message: err
                        });
                        $scope.hide();
                        console.log(err);
                    }).finally(function () {
                        $scope.hide();
                    });
                }
            };
        });
