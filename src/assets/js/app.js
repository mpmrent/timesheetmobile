angular.module('tms', ['ionic', 'tms.controllers', 'tms.services', 'tms.filters','ngCordova'])
        .run(function ($ionicPlatform) {
            $ionicPlatform.ready(function () {
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
                    cordova.plugins.Keyboard.disableScroll(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleLightContent();
                }
            });
        })
        .config(function ($stateProvider, $urlRouterProvider, $compileProvider,$httpProvider) {
            $urlRouterProvider.otherwise('/');
            $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
            $httpProvider.interceptors.push('interceptorAllRequest');
            $stateProvider
                    .state('home', {
                        url: '/',
                        templateUrl: 'assets/pages/logon.html',
                        controller: 'HomeCtrl'
                    })
                    .state('main', {
                        url: '/main',
                        templateUrl: 'assets/pages/main.html',
                        controller: 'MainCtrl'
                    })
                    .state('new', {
                        url: '/new',
                        templateUrl: 'assets/pages/form.html',
                        controller: 'FormCtrl'
                    })
                    .state('report', {
                        url: '/report',
                        templateUrl: 'assets/pages/report.html',
                        controller: 'ReportCtrl'
                    })
                    .state('list', {
                        url: '/list',
                        templateUrl: 'assets/pages/list.html',
                        controller: 'ListCtrl'
                    })
                    .state('listdetail', {
                        url: '/listdetail/:timeId/:status',
                        templateUrl: 'assets/pages/listdetail.html',
                        controller: 'ListDetailCtrl'
                    })
                    .state('formedit', {
                        url: '/formedit/:timeId',
                        templateUrl: 'assets/pages/formedit.html',
                        controller: 'FormEditCtrl'
                    })
                    .state('logout', {
                        url: '/',
                        templateUrl: 'assets/pages/logon.html',
                        controller: 'LogoutCtrl'
                    })
                    .state('camera', {
                        url: "/camera",
                        templateUrl: "assets/pages/camera.html",
                        controller: 'CameraCtrl'
                    })
                    .state('article', {
                        url: "/article",
                        templateUrl: "assets/pages/article.html",
                        controller: 'ArticleCtrl'
                    })
                    .state('articledetail', {
                        url: "/articledetail/:idArticle",
                        templateUrl: "assets/pages/articledetail.html",
                        controller: 'ArticleDetailCtrl'
                    })
                    .state('message', {
                        url: "/message",
                        templateUrl: "assets/pages/message.html",
                        controller: 'MessageCtrl'
                    })
                    .state('messagedetail', {
                        url: "/messagedetail/:idMessage",
                        templateUrl: "assets/pages/messagedetail.html",
                        controller: 'MessageDetailCtrl'
                    })
                    .state('bstklist', {
                        url: '/bstklist',
                        templateUrl: 'assets/pages/bstklist.html',
                        controller: 'BstkListCtrl'
                    })
                    .state('bstk', {
                        url: '/bstk',
                        templateUrl: 'assets/pages/bstk.html',
                        controller: 'BSTKCtrl'
                    })
                    .state('detailbstk', {
                        url: '/detailbstk/:idBSTK',
                        templateUrl: 'assets/pages/bstk.html',
                        controller: 'DetailBSTKCtrl'
                    })
                    .state('editprofile', {
                        url: '/editprofile/:id',
                        templateUrl: 'assets/pages/profile.html',
                        controller: 'ProfileCtrl'
                    });
                    $urlRouterProvider.otherwise('/');
        });
