angular.module('tms.services', [])
        .factory('Camera', ['$q', function($q) {
                return {
                getPicture: function(options) {
                    var q = $q.defer();
        
                    navigator.camera.getPicture(function(result) {
                    // Do any magic you need
                    q.resolve(result);
                    }, function(err) {
                    q.reject(err);
                    }, options);
        
                    return q.promise;
                }
                }
            }])
        .factory('ClockSrv', function($interval){
                var clock = null;
                var service = {
                    startClock: function(fn, time){
                        if(clock === null){
                            clock = $interval(fn, time);
                        }
                    },
                    stopClock: function(){
                        if(clock !== null){
                            $interval.cancel(clock);
                            clock = null;
                        }
                    }
                };
              
                return service;
            })
        .factory('ConnectivityMonitor', function($rootScope, $cordovaNetwork){

          return {
            isOnline: function(){
              if(ionic.Platform.isWebView()){
                return $cordovaNetwork.isOnline();
              } else {
                return navigator.onLine;
              }
            },
            isOffline: function(){
              if(ionic.Platform.isWebView()){
                return !$cordovaNetwork.isOnline();
              } else {
                return !navigator.onLine;
              }
            },
            startWatching: function(){
              if(ionic.Platform.isWebView()){

                $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
                  console.log("went online");
                });

                $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                  console.log("went offline");
                });

              } else {

                window.addEventListener("online", function(e) {
                  console.log("went online");
                }, false);

                window.addEventListener("offline", function(e) {
                  console.log("went offline");
                }, false);
              }
            }
          }
        })
        .factory('interceptorAllRequest', ['ConnectivityMonitor','$injector',function(ConnectivityMonitor,$injector) {
          return {
            request: function(config) {
              if(ConnectivityMonitor.isOffline()){
                var ionicPopup = $injector.get('$ionicPopup');
                var popUp = ionicPopup.alert({
                  title: "Kesalahan !",
                  template: "Error, Tidak bisa terhubung ke jaringan",
                  okType: 'button-assertive',
                  enableBackdropDismiss: true
                });
                popUp.then(function(res) {
                  console.log('something');
                });
              }
              return config;
            }
          };
        }])
        .factory('tmsService', function ($http, $window) {
        
            // var baseUrl = "http://timesheet.mpm-rent.net/tsm_live/index.php/services/";
            // var bstkUrl = "http://timesheet.mpm-rent.net/tsm_live/index.php/e_bstk/";
            var baseUrl = "http://182.23.44.169/tsm_web/index.php/services/";
            var bstkUrl = "http://182.23.44.169/tsm_web/index.php/e_bstk/";
            // var baseUrl = "http://localhost/tsm_server/index.php/services/";
            // var bstkUrl = "http://localhost/tsm_server/index.php/e_bstk/";
            return {
                doLogin: function (data) {
                    return $http.post(baseUrl + 'doLogin', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                doCheckTimesheet: function () {
                    return $http.post(baseUrl + 'doCheckTimesheet/' + $window.localStorage['uid'], {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getDriver: function () {
                    return $http.post(baseUrl + 'get_driver', null, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getProvince: function () {
                    return $http.post(baseUrl + 'get_province', null, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                checkUpdate: function (version) {
                    return $http.post(baseUrl + 'check_update/' + version, null, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                add: function (data) {
                    return $http.post(baseUrl + 'add', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                checkCountry: function (data) {
                    return $http.get(baseUrl + 'get_location', data, {
                        headers: {
                            'Access-Control-Allow-Origin' : '*',
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                addLocation: function () {
                    return $http.post(baseUrl + 'addLocation', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                approve: function (data) {
                    return $http.post(baseUrl + 'approve', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                update: function (data) {
                    return $http.post(baseUrl + 'update', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getAllTimesheetNotApprove: function () {
                    return $http.get(baseUrl + 'listsNotApprove/' + $window.localStorage['uid']);
                },
                getAllTimesheet: function () {
                    return $http.get(baseUrl + 'lists/' + $window.localStorage['uid']);
                },
                getIdTimesheet: function (id) {
                    return $http.get(baseUrl + 'list_details/' + id);
                },
                getAllReport: function () {
                    return $http.get(baseUrl + 'approved_lists/' + $window.localStorage['uid']);
                },
                getIdReport: function (id) {
                    return $http.get(baseUrl + 'approved_list_details/' + id);
                },
                deleteTime: function (data) {
                    return $http.post(baseUrl + 'delete_batch', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                searchReport: function (data) {
                    return $http.post(baseUrl + 'approved_lists_period', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                searchTimesheet: function (data) {
                    return $http.post(baseUrl + 'lists_period', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getPICApprove: function (data) {
                    return $http.post(baseUrl + 'get_pic_approve', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getArticles: function (data) {
                    return $http.post(baseUrl + 'getArticles/' + data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getArticleDetail: function (data) {
                    return $http.get(baseUrl + 'getArticleDetail/' + data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getMessage: function (data) {
                    return $http.get(baseUrl + 'getMessage/' + data + '/1', {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getUnreadMessage: function (data) {
                    return $http.get(baseUrl + 'getUnreadMessage/' + data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getAllMessage: function (data) {
                    return $http.get(baseUrl + 'getAllMessage/' + data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getMessageDetail: function (data) {
                    return $http.get(baseUrl + 'getMessageDetail/' + data.user + '/' + data.id, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                // getChecklist: function () {
                //     return $http.get(bstkUrl + 'main/get_checklist', {
                //         headers: {
                //             'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                //         }
                //     });
                // },
                getChecklist: function () {
                    return $http.post(bstkUrl + 'main/get_checklist', {
                        headers: {
                            'Access-Control-Allow-Origin' : '*',
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getAllInsp: function () {
                    return $http.post(bstkUrl + 'main/get_all_insp', {
                        headers: {
                            'Access-Control-Allow-Origin' : '*',
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getBSTKDetail: function (data) {
                    return $http.post(bstkUrl + 'main/get_id_insp/' + data.id, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getDataDriver: function (id) {
                    return $http.post(baseUrl + 'getDataDriver/' + id, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                updateDataDriver: function (data) {
                    return $http.post(baseUrl + 'updateDataDriver', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                getAgree: function (data) {
                    return $http.post(baseUrl + 'agree', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
                cekAgree: function (data) {
                    return $http.post(baseUrl + 'cekagree', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
                        }
                    });
                },
            };
        });
