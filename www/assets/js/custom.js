FCMPlugin.onNotification(
    function(data){ //callback
      if(data.wasTapped){
        //Notification was received on device tray and tapped by the user.
        console.log( JSON.stringify(data) );
      } else {
        //Notification was received in foreground. Maybe the user needs to be notified.
        console.log( JSON.stringify(data) );
      }
    },
    function(msg){ //success handler
      console.log('onNotification callback successfully registered: ' + msg);
    },
    function(err){ //error handler
      console.log('Error registering onNotification callback: ' + err);
    }
);